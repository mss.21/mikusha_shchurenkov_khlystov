﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    internal class Infrastructure
    {
        public int road_condition;

        public int buildings_condition;


        public Infrastructure(int road_condition, int buildings_condition)
        {
            this.road_condition = road_condition;
            this.buildings_condition = buildings_condition;
        }

        public int Road(int answer)
        {
            if (answer == 0)
            {
                road_condition -= 1;

            }
            if (answer == 1)
            {
                road_condition += 1;

            }
            return road_condition;
        }
        public int Building(int answer)
        {
            if (answer == 0)
            {
                buildings_condition -= 1;
            }
            if (answer == 1)
            {
                buildings_condition += 1;
            }
            return buildings_condition;

        }
    }
}
