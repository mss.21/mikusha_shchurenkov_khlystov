﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

internal class Politics
{
    public int riot_condition;
    public int war_condition;

    public Politics(int riot_condition, int war_condition)
    {
        this.riot_condition = riot_condition;
        this.war_condition = war_condition;
    }
    public int People(int answer)
    {
        if (answer == 0)
        {
            riot_condition += 1;
        }
        if (answer == 1)
        {
            riot_condition -= 1;
        }
        return riot_condition;
    }

    public int Neighbours(int answer)
    {
        if (answer == 0)
        {
            war_condition += 1;
        }
        if (answer == 1)
        {
            war_condition -= 1;
        }
        return war_condition;

    }
}
