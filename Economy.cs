﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

internal class Economy
{
    public int budget_condition;

    public Economy(int budget_condition)
    {
        this.budget_condition = budget_condition;
    }
    public int Budget(int answer)
    {
        if (answer == 0)
        {
            budget_condition -= 1;
        }
        if (answer == 1)
        {
            budget_condition += 1;
        }
        return budget_condition;
    }
}
