﻿using System;

namespace ConsoleApp12
{
    class Program
    {
        static void Main(string[] args)
        {

            int riot_condition = 0;
            int war_condition = 0;


            int road_condition = 10;
            int buildings_condition = 10;

            int budget_condition = 10;

            int answer;

            Politics politics = new Politics(riot_condition, war_condition);
            Infrastructure infrastructure = new Infrastructure(road_condition, buildings_condition);
            Economy economy = new Economy(budget_condition);

            Console.WriteLine("Мой Король, сегодня у нас много приёмов! Вы готовы начать? Да (1), нет (0).");
            answer = Convert.ToInt32(Console.ReadLine());
            if (answer == 0)
            {
                Console.WriteLine("Сегодня вы решили не заниматься государственными делами и отправились спать.");
            }
            else if (answer == 1)
            {
                {
                    Console.WriteLine("1. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает заключить нашим государствам союз. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }
                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("2. *В зал входит cенатор*. Уважаемый Король! Народ требует снижений налогов." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer-1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("3. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает выступить в войне против *государство-нейм*. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("4. *В зал входит cенатор*. Уважаемый Король! Народ требует улучшения качества жилья." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        buildings_condition = infrastructure.Road(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("5. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь требует не размещать военных у границы. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("6. *В зал входит cенатор*. Уважаемый Король! Народ требует увеличения рабочих мест." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("7. *В зал входит купец*. Уважаемый Король! Разрешите нам экспорт в другие страны." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        budget_condition = economy.Budget(answer);
                    }
                    else if (answer == 0)
                    {
                        budget_condition = economy.Budget(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("8. *В зал входит cенатор*. Уважаемый Король! Народ требует улучшения качества дорог." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                        road_condition = infrastructure.Road(answer);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("9. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает создать торговый путь между столицами. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        road_condition = infrastructure.Road(answer);
                        budget_condition = economy.Budget(answer);
                        war_condition = politics.Neighbours(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("10. *В зал входит купец*. Уважаемый Король! Можете ли вы снизить налоги на импорт?" +
                    "Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}, " +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        budget_condition = economy.Budget(answer);
                    }
                    else if (answer == 0)
                    {
                        budget_condition = economy.Budget(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("11. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает открыть границы друг для друга. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("12. *В зал входит cенатор*. Уважаемый Король! Народ требует снижение цен на коменальные услуги." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                        buildings_condition = infrastructure.Building(answer);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("13. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает создать в научный союз. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                        road_condition = infrastructure.Road(answer);
                        buildings_condition = infrastructure.Building(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("14. *В зал входит cенатор*. Уважаемый Король! Народ требует уменьшение рабочего времени." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("15. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает укрепить торговые связи. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                        budget_condition = economy.Budget(answer);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("16. *В зал входит cенатор*. Уважаемый Король! Народ требует бесплатную медицинскую помощь." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("17. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает создать военный союз. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("18. *В зал входит cенатор*. Уважаемый Король! Народ требует увеличения заработной платы." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("19. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает открыть посольства во всех крупных городах Вашей державы. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("20. *В зал входит cенатор*. Уважаемый Король! Народ требует соц. поддержки." +
                    "Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        riot_condition = politics.People(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        riot_condition = politics.People(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                {
                    Console.WriteLine("21. *В зал входит посол соседнего государства*. Уважаемый Король! " +
                        "Наш Государь предлагает создать демилитаризованную зону на некоторых участках границы. Вы согласны? Да (1), нет (0).");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                    {
                        Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}" +
                            $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                    }
                    else if (answer == 1)
                    {
                        war_condition = politics.Neighbours(answer);
                        budget_condition = economy.Budget(answer - 1);
                    }
                    else if (answer == 0)
                    {
                        war_condition = politics.Neighbours(answer);
                    }
                }

                politics = new Politics(riot_condition, war_condition);
                infrastructure = new Infrastructure(road_condition, buildings_condition);
                economy = new Economy(budget_condition);

                if (riot_condition > 10 | war_condition >= 10 | budget_condition < 0)
                {
                    Console.WriteLine($"Игра окончена! Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}, " +
                        $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                }
                else if (riot_condition < 10 | war_condition <= 10 | budget_condition > 0)
                {
                    Console.WriteLine($"Конец! Вы по истине достойны звания великого Короля. Ваша статистика: состояние бюджета - {budget_condition}, состояние бунта - {riot_condition}, " +
                        $"состояние войны - {war_condition}, состояние дорог - {road_condition}, состояние зданий {buildings_condition}");
                }
            }
        }
    }
}